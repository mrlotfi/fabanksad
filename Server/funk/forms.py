from dal import autocomplete
from django.contrib.auth.forms import UserCreationForm
from django.forms.widgets import Textarea

from funk.models import Branch, Employee, Customer, Account
from django.forms import ModelForm, ModelChoiceField

class BranchCreationForm(ModelForm):
    class Meta:
        model = Branch
        fields = ['name', 'address', 'total_available_money']
        widgets = {
            'address': Textarea(attrs={'rows':4})
        }
        labels = {
            'name': ('نام شعبه'),
            'address': 'آدرس شعبه (۵۰۰ حرف)',
            'total_available_money': 'موجودی اولیه شعبه'
        }


class BranchEditForm(ModelForm):
    class Meta:
        model = Branch
        fields = ['name', 'address']
        labels = {
            'name': 'نام شعبه',
            'address': 'آدرس شعبه'
        }


class BranchHeadCreationForm(UserCreationForm):
    class Meta:
        model = Employee
        fields = ['national_id', 'name', 'family', 'username', 'password1', 'password2', 'branch']
        labels = {
            'national_id': 'کد ملی',
            'name': 'نام',
            'family': 'نام خانوادگی',
            'branch': 'شعبه'
        }
    branch = ModelChoiceField(queryset=Branch.objects.
                              filter(activation_status=True), label='انتخاب شعبه')


class EmployeeCreationForm(UserCreationForm):
    class Meta:
        model = Employee
        fields = ['national_id', 'name', 'family', 'username', 'role', 'password1', 'password2', 'branch']
        labels = {
            'national_id': 'کد ملی',
            'name': 'نام',
            'family': 'نام خانوادگی',
            'branch': 'شعبه',
            'role': 'نقش'
        }
    branch = ModelChoiceField(queryset=Branch.objects.
                              filter(activation_status=True), label='انتخاب شعبه')


class CustomerCreationForm(UserCreationForm):
    class Meta:
        model = Customer
        fields = ['national_id', 'first_name', 'last_name', 'username', 'password1', 'password2']



class AccountCreationForm(ModelForm):
    class Meta:
        model = Account
        fields = ['customer']
        labels = {
            'customer': 'انتخاب مشتری جهت ایجاد حساب'
        }
        widgets = {
            'customer': autocomplete.ModelSelect2(url='funk:search_user_ajax', attrs={'data-html': 'true'})
        }

    def clean(self):
        cleaned_data = super(AccountCreationForm, self).clean()
        customer = cleaned_data.get('customer')
        if not customer.activation_status:
            self.add_error('customer', 'حساب کاربری مشتری مورد نظر تایید حقوقی نشده است')
        return  cleaned_data


class AccountSearchForm(ModelForm):
    class Meta:
        model = Account
        fields = ['customer',]

        widgets = {
            'customer': autocomplete.ModelSelect2(url='funk:search_user_ajax', attrs={'data-html': 'true'})
        }
