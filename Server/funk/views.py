from dal import autocomplete
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.db.models.query_utils import Q
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.urls.base import  reverse_lazy
from django.views.generic.base import View
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

from funk.forms import BranchCreationForm, BranchEditForm, BranchHeadCreationForm, EmployeeCreationForm, \
    CustomerCreationForm, AccountCreationForm, AccountSearchForm
from funk.models import Branch, Employee, Customer, Account


class AddUserToContextMixin(object):
    def get_context_data(self, **kwargs):
        c = super(AddUserToContextMixin, self).get_context_data(**kwargs)
        try:
            c['user_info'] = Employee.objects.get(pk=self.request.user.id)
        except:
            c['user_info'] = 'chrt'
        return c


class ShowDash(View):
    def get(self, request):
        print(request.GET.get('message'))
        try:
            user = Employee.objects.get(pk=self.request.user.id)
        except:
            user = 'chrt'
        message = request.GET.get('message', 'خوش آمدید')
        return render(request, 'funk/Dash.html', context={'user_info':user, 'message': message})


class CreateBranch(AddUserToContextMixin, CreateView):
    form_class = BranchCreationForm
    template_name = 'funk/BranchTemplates/BranchCreation.html'
    success_url = '/dash?message='+ 'ثبت شعبه با موفقیت انجام شد'

    def form_valid(self, form):
        m = form.save(commit=False)
        m.activation_status = True
        return super(CreateBranch, self).form_valid(form)



class SearchBranch(AddUserToContextMixin, ListView):
    model = Branch
    context_object_name = 'result'
    template_name = 'funk/BranchTemplates/BranchSearch.html'

    def get_queryset(self):
        branch_name = self.request.GET.get('branch_name', '')
        return Branch.objects.filter(name__icontains=branch_name).filter(activation_status=True)


class RemoveBranch(AddUserToContextMixin, View):
    def get(self, request, *args, **kwargs):
        try:
            user = Employee.objects.get(pk=self.request.user.id)
        except:
            user = 'chrt'
        return render(request, 'funk/BranchTemplates/BranchRemove.html', context={'user_info': user})

    def post(self, request, *args, **kwargs):
        branch = Branch.objects.get(pk=self.kwargs['branch_id'])
        branch.activation_status = False
        branch.save()
        return HttpResponseRedirect('/dash?message='+'حذف شعبه موفقیت آمیز بود')


class EditBranch(AddUserToContextMixin, UpdateView):
    form_class = BranchEditForm
    success_url = '/dash?message='+'به روز رسانی شعبه موفقیت آمیز بود'
    template_name = 'funk/BranchTemplates/BranchEdit.html'

    def get_object(self, queryset=None):
        return Branch.objects.get(pk=self.kwargs['branch_id'])


class CreateBranchHead(AddUserToContextMixin, CreateView):
    form_class = BranchHeadCreationForm
    template_name = 'funk/BranchHeadTemplates/BranchHeadCreation.html'
    success_url = '/dash?message=' + 'ثبت رئیس شعبه با موفقیت انجام شد'

    def form_valid(self, form):
        temp_form = form.save(commit=False)
        temp_form.role = Employee.BRANCH_HEAD_CODE
        return super(CreateBranchHead, self).form_valid(form)


class SearchBranchHead(AddUserToContextMixin, ListView):
    model = Employee
    context_object_name = 'result'
    template_name = 'funk/BranchHeadTemplates/BranchHeadSearch.html'

    def get_queryset(self):
        nid = self.request.GET.get('nid', '')
        name = self.request.GET.get('name', '')
        return Employee.objects.filter(national_id__icontains=nid).filter(name__icontains=name).filter(role=Employee.BRANCH_HEAD_CODE)


class EditBranchHead(AddUserToContextMixin, UpdateView):
    form_class = BranchHeadCreationForm
    template_name = 'funk/BranchHeadTemplates/BranchHeadEdit.html'
    success_url = '/dash?message='+ 'ویرایش رئیس شعبه با موفقیت انجام شد'

    def get_object(self, queryset=None):
        return Employee.objects.get(pk=self.kwargs['head_id'])


class RemoveBranchHead(AddUserToContextMixin, View):
    def get(self, request, *args, **kwargs):
        try:
            user = Employee.objects.get(pk=self.request.user.id)
        except:
            user = 'chrt'
        return render(request, 'funk/BranchHeadTemplates/BranchHeadRemove.html', context={'user_info': user})

    def post(self, request, *args, **kwargs):
        Employee.objects.get(pk=self.kwargs['head_id']).delete()
        return HttpResponseRedirect('/dash?message='+'حذف  رییس شعبه موفقیت آمیز بود')


class CreateEmployee(AddUserToContextMixin, CreateView):
    form_class = EmployeeCreationForm
    template_name = 'funk/BranchManagerTemplates/EmployeeCreationEdit.html'
    success_url = '/dash?message='+ 'کارمند شعبه با موفقیت ثبت شد!'

    def form_valid(self, form):
        temp_form = form.save(commit=False)
        user = Employee.objects.get(pk=self.request.user.id)
        if user.role != Employee.TOTAL_MANAGER_CODE:
            temp_form.branch = user.branch
        return super(CreateEmployee, self).form_valid(form)


class SearchEmployee(AddUserToContextMixin, ListView):
    model = Employee
    context_object_name = 'result'
    template_name = 'funk/BranchManagerTemplates/EmployeeSearch.html'

    def get_queryset(self):
        nid = self.request.GET.get('nid', '')
        name = self.request.GET.get('name', '')
        return Employee.objects.filter(national_id__icontains=nid).filter(name__icontains=name).exclude(role=Employee.BRANCH_HEAD_CODE).exclude(role=Employee.TOTAL_MANAGER_CODE)

class EditEmployee(AddUserToContextMixin, UpdateView):
    form_class = EmployeeCreationForm
    template_name = 'funk/BranchManagerTemplates/EmployeeCreationEdit.html'
    success_url = '/dash?message=' + 'کارمند شعبه با موفقیت ویرایش شد!'

    def form_valid(self, form):
        temp_form = form.save(commit=False)
        user = Employee.objects.get(pk=self.request.user.id)
        if user.role != Employee.TOTAL_MANAGER_CODE:
            temp_form.branch = user.branch
        return super(EditEmployee, self).form_valid(form)

    def get_object(self, queryset=None):
        return Employee.objects.get(pk=self.kwargs['employee_id'])


class RemoveEmployee(AddUserToContextMixin, View):
    def get(self, request, *args, **kwargs):
        try:
            user = Employee.objects.get(pk=self.request.user.id)
        except:
            user = 'chrt'
        return render(request, 'funk/BranchManagerTemplates/EmployeeRemove.html', context={'user_info': user})

    def post(self, request, *args, **kwargs):
        Employee.objects.get(pk=self.kwargs['employee_id']).delete()
        return HttpResponseRedirect('/dash?message='+'حذف  کارمند شعبه موفقیت آمیز بود')


class CreateCustomer(CreateView):
    form_class = CustomerCreationForm
    template_name = 'funk/CustomerTemplates/CustomerCreation.html'
    success_url = '/dash?message='+'ثبت موفق مشتری جدید'


class SearchCustomer(CreateView, ListView):
    model = Customer
    fields = ['first_name', 'last_name', 'activation_status']
    context_object_name = 'result'
    template_name = 'funk/CustomerTemplates/SearchCustomer.html'
    def get_queryset(self):
        form_class = self.get_form_class()
        form = form_class(self.request.GET)
        if not form.is_valid():
            raise ValidationError('yuhahaha')
        first_level_filtered_customers =  Customer.objects.filter(first_name__icontains=form.cleaned_data['first_name']).filter(
            last_name__icontains=form.cleaned_data['last_name']
        )
        if form.cleaned_data['activation_status'] == False:
            first_level_filtered_customers = first_level_filtered_customers.filter(activation_status='False')
        return first_level_filtered_customers
    def post(self, request, *args, **kwargs):
        raise ValidationError('Go fuck yourself')


class ActivateCustomer(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'funk/CustomerTemplates/CustomerActivation.html')

    def post(self, request, *args, **kwargs):
        c = Customer.objects.get(pk=self.kwargs['customder_id'])
        c.activation_status = True
        c.save()
        return HttpResponseRedirect(reverse_lazy('funk:search_customer'))

class CreateAccount(CreateView):
    form_class = AccountCreationForm
    template_name = 'funk/AccountTemplates/AccountCreation.html'

    def get_success_url(self):
        return '/dash?message='+'حساب جدید ثبت شد. شماره حساب ایجاد شده: '+str(self.object.id) + 'برای مشتری: ' + self.object.customer.username


class BanSearchAccount(CreateView, ListView):
    form_class = AccountSearchForm
    context_object_name = 'result'
    template_name = 'funk/AccountTemplates/AccountBanSearch.html'

    def get_queryset(self):
        form_class = self.get_form_class()
        form = form_class(self.request.GET)
        initial_accounts = Account.objects.filter(closed_by_customer=False)
        if form.is_valid() and form.cleaned_data['customer']:
            initial_accounts = initial_accounts.filter(customer=form.cleaned_data['customer'])
        if self.request.GET.get('id',''):
            initial_accounts = initial_accounts.filter(pk=self.request.GET.get('id'))
        if self.request.GET.get('banned',''):
            initial_accounts = initial_accounts.filter(ban_status=True)
        else:
            initial_accounts = initial_accounts.filter(ban_status=False)
        return initial_accounts

    def post(self, request, *args, **kwargs):
        raise ValidationError('Go fuck yourself')

class BanAccount(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'funk/AccountTemplates/AccountBan.html')

    def post(self, request, *args, **kwargs):
        c = Account.objects.get(pk=self.kwargs['account_id'])
        if self.kwargs['ban_unban']:
            c.ban_status = True
        else:
            c.ban_status = False
        c.save()
        return HttpResponseRedirect('/dash?message='+'مسدود/باز کردن حساب موفقیت آمیز بود')

class SearchCustomerAjax(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        q = self.q
        return Customer.objects.filter(Q(national_id__icontains=q)| Q(first_name__icontains=q) | Q(last_name__icontains=q)
                                       | Q(username__icontains=q) )

    def get_result_label(self, result):
        base_div = '<div>{label}: {text}</div>'
        id = base_div.format(label='شماره کاربر', text=result.id)
        national_id = base_div.format(label='شماره ملی', text=result.national_id)
        username = base_div.format(label='نام کاربری', text=result.username)
        name = base_div.format(label='نام و نام خانوادگی',text=result.first_name + result.last_name)
        return id+national_id+ username+ name