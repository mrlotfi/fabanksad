from django.contrib.auth.models import User
from django.db import models

# Create your models here.
# TODO single app or multiapp?


class Branch(models.Model):
    name = models.CharField(max_length=200)
    address = models.TextField(max_length=500)
    activation_status = models.BooleanField()
    total_available_money = models.BigIntegerField()

    def __str__(self):
        return 'نام شعبه: '+ self.name

class Employee(User):
    national_id = models.CharField(null=False, max_length=20)
    name = models.CharField(max_length=30)
    family = models.CharField(max_length=50)
    TOTAL_MANAGER_CODE = 1
    BRANCH_HEAD_CODE = 2
    BRANCH_TAHVILDAR = 3
    ROLES = (
        # (TOTAL_MANAGER_CODE, 'مدیر کل'),
        # (BRANCH_HEAD_CODE, 'رئیس شعبه'),
        (BRANCH_TAHVILDAR, 'تحویلدار'),

    )
    role = models.SmallIntegerField(choices=ROLES)
    branch = models.ForeignKey(Branch, blank=False, null=True)


class Customer(User):
    national_id = models.CharField('کد ملی',null=False, max_length=20)
    activation_status = models.BooleanField('فعال سازی شده', default=False, blank=True)


class Account(models.Model):  # use id as account number
    employee = models.ForeignKey(Employee, null=True)
    branch = models.ForeignKey(Branch, null=True)
    customer = models.ForeignKey(Customer, blank=True)
    balance = models.IntegerField(default=0)
    ban_status = models.BooleanField(default=False)
    closed_by_customer = models.BooleanField(default=False)
    opening_time = models.DateTimeField(auto_now_add=True)
    closing_time = models.DateTimeField(null=True)


class Atm(models.Model):
    branch = models.ForeignKey(Branch)
    balance_limit = models.IntegerField(default=0)


class DepositeOrWithdraw(models.Model): #wtf is this name
    amount = models.IntegerField()
    account = models.ForeignKey(Account)
    employee = models.ForeignKey(Employee, blank=True)
    # atm = models.ForeignKey(Atm, blank=True)
    time = models.DateTimeField()
    payer_national_id = models.CharField(max_length=20)
    surcharge = models.IntegerField(default=0)
    type = models.BooleanField()  # 0: deposite -- 1: withdraw


# class Transfer(models.Model):
#     source_account = models.ForeignKey(Account)
#     destination_account = models.ForeignKey(Account)
#     amount = models.IntegerField()
#     time = models.DateTimeField()
#     employee = models.ForeignKey(Employee, blank=True)
#     # atm = models.ForeignKey(Atm, blank=True)
#     surcharge = models.IntegerField(default=0)
#     is_interest = models.BooleanField()


