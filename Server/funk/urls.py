from django.contrib.auth.views import login as login_view
from django.contrib.auth.views import logout as logout_view
from django.conf.urls import url
from django.views.generic.base import TemplateView

from funk.views import CreateBranch, ShowDash, SearchBranch, RemoveBranch, EditBranch, CreateBranchHead, \
    SearchBranchHead, EditBranchHead, RemoveBranchHead, CreateEmployee, SearchEmployee, EditEmployee, RemoveEmployee, \
    CreateCustomer, SearchCustomer, ActivateCustomer, CreateAccount, SearchCustomerAjax, BanSearchAccount, BanAccount

app_name = 'funk'
urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='funk/Welcome.html'), name='home'),

    url(r'^ajax/usersearch/$', SearchCustomerAjax.as_view(), name='search_user_ajax'),

    url(r'^login/$', login_view, {'template_name': 'funk/login.html'},  name='login'),
    url(r'^logout/$', logout_view,{'next_page': '/'}, name='logout'),
    url(r'^dash/$', ShowDash.as_view(), name='dash'),

    url(r'^dash/branch/creation/$', CreateBranch.as_view(), name='branch_creation'),
    url(r'^dash/branch/search/$', SearchBranch.as_view(), name='branch_search'),
    url(r'^dash/branch/delete/(?P<branch_id>[0-9]+)/$', RemoveBranch.as_view(), name='branch_delete'),
    url(r'^dash/branch/edit/(?P<branch_id>[0-9]+)/$', EditBranch.as_view(), name='branch_edit'),


    url(r'^dash/employee/head/creation/$', CreateBranchHead.as_view(), name='branch_head_creation'),
    url(r'^dash/employee/head/search/$', SearchBranchHead.as_view(), name='branch_head_search'),
    url(r'^dash/employee/head/edit/(?P<head_id>[0-9]+)/$', EditBranchHead.as_view(), name='branch_head_edit'),
    url(r'^dash/employee/head/delete/(?P<head_id>[0-9]+)/$', RemoveBranchHead.as_view(), name='branch_head_delete'),

    url(r'^dash/employee/creation/$', CreateEmployee.as_view(), name='employee_creation'),
    url(r'^dash/employee/search/$', SearchEmployee.as_view(), name='employee_search'),
    url(r'^dash/employee/edit/(?P<employee_id>[0-9]+)/$', EditEmployee.as_view(), name='employee_edit'),
    url(r'^dash/employee/delete/(?P<employee_id>[0-9]+)/$', RemoveEmployee.as_view(), name='employee_delete'),

    url(r'^dash/customer/creation/$', CreateCustomer.as_view(), name='cusomter_creation'),
    url(r'^dash/customer/search/$', SearchCustomer.as_view(), name='search_customer'),
    url(r'^dash/customer/(?P<customder_id>[0-9]+)/activate/$', ActivateCustomer.as_view(), name='activate_customer'),

    url(r'^dash/account/creation/$', CreateAccount.as_view(), name='account_creation'),
    url(r'^dash/account/ban/search/$', BanSearchAccount.as_view(), name='account_ban_search'),
    url(r'^dash/account/ban/(?P<account_id>[0-9]+)/(?P<ban_unban>[01])/$', BanAccount.as_view(), name='account_ban')
]